function mainpage() {
    // var windowWidth = jQuery(window).width();
    // var widthContainer = $('.container').width();
    // var windowHeight = $(window).height();
    // var heightHeader = $('header').height();
    // var heightFooter = $('footer').height();
    // $(".main-page").css("min-height", windowHeight - heightHeader - heightFooter);
}

$(document).ready(function () {
    $(".sso-form-main .sso-form-control").focus(function () {
        $(this).parent().addClass("onFocus")
        $(this).parent().addClass("active")
    });
    $(".sso-form-main .sso-form-control").blur(function () {
        $(this).parent().removeClass("onFocus")
        if(!($(this).val())) {
            $(this).parent().removeClass("active")
        }
    });

    mainpage();
    jQuery(window).resize(function () {
        mainpage();
    });

});


